PROD_IMG_NAME ?= imgo:prod
DEV_IMG_NAME ?= imgo:dev

.PHONY: build-prod
build-prod:
	docker build --target basebuild -t ${PROD_IMG_NAME} .

.PHONY: build-dev
build-dev:
	docker build --target dev -t ${DEV_IMG_NAME} .