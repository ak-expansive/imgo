
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    name: web
  name: web
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      name: web
  template:
    metadata:
      labels:
        name: web
      name: web
    spec:
      imagePullSecrets:
      - name: gitlabpullsecret
      volumes:
      - name: imgo-pv-storage
        persistentVolumeClaim:
          claimName: imgo-pv-claim
      {{- if eq .dev_mode "true" }}
      - name: app-volume
        hostPath:
          path: {{ .host_path }}/src
      {{- end }}
      containers:
      {{- if eq .dev_mode "true" }}
      - image: "registry.gitlab.com/ak-expansive/imgo/dev:latest"
      {{- else }}
      - image: "registry.gitlab.com/ak-expansive/imgo:latest"
      {{- end}}
        imagePullPolicy: IfNotPresent
        name: imgo
        {{- if eq .dev_mode "true" }}
        workingDir: "/go/src/app-dev"
        {{- end }}
        volumeMounts:
        {{- if eq .dev_mode "true" }}
        - name: app-volume
          mountPath: "/go/src/app-dev"
        {{- end }}
        - name: imgo-pv-storage
          mountPath: "/opt/images"
        ports:
        - containerPort: 8080
        env:
        - name: IMGO_IMG_DIR
          value: "/opt/images"
        - name: IMGO_PORT
          value: "8080"
        - name: IMGO_FORCE_HTTPS
          value: "true"
        resources: ~
