# Imgo

A simple image server written in golang. This is designed for use in dev environments as a basic CDN.

It will implement the ability to upload an image, and then serve said image; nothing more complex.

## Hot Recompilation

The dev variant of this image uses hot recompilation via [CompileDaemon](https://github.com/githubnemo/CompileDaemon). This automatically recompiles the app and restarts the server whenever changes are detected in the src directory. 

You may find problems with this tool when using NFS and it relies on [fsnotify](https://github.com/fsnotify/fsnotify). NFS does not actually cause file change notifications, meaning that the recompilation will never happen even though the files are updated.

To get around this I've introduced the `./copy-changes.sh` script, which acts as a watcher of the app code. The code should be mounted via NFS into `/go/src/app-dev`, and the files will be synced through usual NFS processes. This script essentially concatenates all the files in the src directory into one and then base 64 encodes it; this is used to determine whether any files have changed since it last `synced` the `/go/src/app-dev` and `/go/src/app-build` directories. Using the `diff` and `wc` commands, it determines whether any changes have occurred since the last run. When file changes have occurred, it uses `cp` to copy the files from `/go/src/app-dev` (the NFS synced directory) into the `/go/src/app-build` directory. As this happens on the native filesystem of the container, the relevant filesystem notifications are fired and CompileDaemon will start a rebuild and restart of the process.

> Unfortunately if we just copy the contents say every 5 seconds into the build directory, CompileDaemon will constantly rebuild and restart the app. This would be a huge pain in the arse when developing. This is why we need to check for changes manually.