FROM golang:1.14 AS basebuild

RUN mkdir /opt/images
WORKDIR /go/src/app
COPY ./src .

RUN go mod download

RUN go build -o /go/bin/imgo

CMD ["imgo"]

FROM basebuild AS dev

RUN mkdir /go/src/app-dev
VOLUME /go/src/app-dev
WORKDIR /go/src/app-dev

RUN go get github.com/githubnemo/CompileDaemon

COPY ./dev-entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY ./copy-changes.sh /copy-changes.sh
RUN chmod +x /copy-changes.sh

ENTRYPOINT ["/entrypoint.sh"]

