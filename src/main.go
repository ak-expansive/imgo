package main

import (
	"fmt"
	"log"
	"os"
	"io"
	"net/http"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"github.com/google/uuid"
	"path/filepath"
	"strings"
	"errors"
	"strconv"
	"github.com/gorilla/handlers"
)


const MaxUUIDGenerationAttempts int = 5

func runServer(c *cli.Context) error {
	err := ValidateEnv()

	if err != nil {
		return err
	}

	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Imgo is running.")
	})

	r.PathPrefix("/img/").Handler(http.StripPrefix("/img", http.FileServer(http.Dir("/opt/images"))))

	r.Path("/upload").Methods("PUT").HandlerFunc(UploadFile)

	fmt.Println("Server listening!")

	// We've already validated the env...shouldn't need to check for an error here...
	port, _ := GetEnv(PortEnvKey)

	handler := handlers.RecoveryHandler()(handlers.CombinedLoggingHandler(os.Stdout, r))

	return http.ListenAndServe(":" + port, handler)
}

func getNewFilePath(filename string, attempt int) (string, string) {
	newName, err := uuid.NewRandom()

	if err != nil {
		panic(err)
	}

	imgDir, err := GetEnv(ImgDirEnvKey)

	if err != nil {
		panic(err)
	}

	// Ensure it always has a trailing slash
	imgDir = strings.TrimSuffix(imgDir, "/") + "/"

	newFilename := newName.String() + filepath.Ext(filename)
	newPath :=  imgDir + newFilename

	_, err = os.Stat(newPath)

	if attempt > MaxUUIDGenerationAttempts {
		panic(errors.New("exceeded maximum tries to generate uuid: " + strconv.Itoa(MaxUUIDGenerationAttempts)))
	}

	if !os.IsNotExist(err) {
		// It exists, we recall this for another uuid

		return getNewFilePath(filename, attempt + 1)
	}

	return newPath, newFilename
}

func getScheme() string {
	scheme := "http"

	shouldForceHttps, err := GetEnv(ForceHttps)

	if err != nil {
		panic(err)
	}

	if shouldForceHttps == "true" {
		scheme = "https"
	}

	return scheme
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	file, handler, err := r.FormFile("image")

	if err != nil {
			panic(err)
	}
	defer file.Close()

	newPath, newName := getNewFilePath(handler.Filename, 1)

	f, err := os.OpenFile(newPath, os.O_WRONLY|os.O_CREATE, 0666)

	if err != nil {
			panic(err)
	}
	defer f.Close()
	_, _ = io.Copy(f, file)


	scheme := getScheme()

	w.Header().Add("Location", scheme + "://" + r.Host + "/img/" + newName)
	w.WriteHeader(http.StatusCreated)
}

func main() {

	app := &cli.App{
    Name: "serve",
		Usage: "Run the imgo server",
		Commands: []*cli.Command{
      {
        Name:    "serve",
        Usage:   "Run the imgo server",
        Action:  runServer,
			},
			{
				Name: "env",
				Usage: "Validate and print the env vars used for configuration",
				Action: func(c *cli.Context) error {
					return ValidateEnv()
				},
			},
    },
    Action: runServer,
  }

	err := app.Run(os.Args)

  if err != nil {
    log.Fatal(err)
	}
}
