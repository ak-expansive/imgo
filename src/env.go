package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)


type validatorFunc func(string) error

// PortEnvKey ...
const PortEnvKey = "IMGO_PORT"

// ImgDirEnvKey ...
const ImgDirEnvKey = "IMGO_IMG_DIR"

const ForceHttps = "IMGO_FORCE_HTTPS"

var requiredEnvVars = map[string]validatorFunc{
	ImgDirEnvKey: func(value string) error {
		file, err := os.Stat(value)

		if err != nil {
			return err
		}

		mode := file.Mode();

		if mode.IsDir() {
			return nil
		}
		
		return errors.New("exists, but is not a directory")
	},
	PortEnvKey: func(value string) error {
		valueAsInt, err := strconv.Atoi(value)

		if err != nil {
			return err
		}

		if valueAsInt < 0 || valueAsInt > 65535 {
			return errors.New("should be between 0 - 65535")
		}

		return nil
	},
	ForceHttps: func(value string) error {
		if value == "true" || value == "false" {
			return nil
		}

		return errors.New("should be 'true' or 'false'")
	},
}

func getErrors() map[string]string {
	errors := make(map[string]string)
	for varName, validator := range requiredEnvVars {

		value, ok := os.LookupEnv(varName)

		if !ok {
			errors[varName] = "Not set."
			continue
		}

		validationError := validator(value)

		if validationError != nil {
			errors[varName] = validationError.Error()
		}
	}

	return errors
}

// Get ...
func GetEnv(envVar string) (string, error) {
	value, exists := os.LookupEnv(envVar)

	if !exists {
		return "", errors.New(envVar + " not found.")
	}

	return value, requiredEnvVars[envVar](value)
}

// Validate ...
func ValidateEnv() error {

	validationErrors := getErrors()

	if len(validationErrors) == 0 {
		for name := range requiredEnvVars {
			fmt.Printf("%s: %s\n", name, os.Getenv(name))
		}

		return nil
	}

	for name, value := range validationErrors {
		fmt.Printf("Invalid value for %s, error: %s\n", name, value)
	}

	return errors.New("invalid environment configuration")
}
